import React, {useCallback, useState} from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import {makeStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import {ReactComponent as GitLabIcon} from './assets/gitlab-logo.svg';
import {useAnalyticsTracking} from './hooks/AnalyticsTracking';
import {LeftEye, RightEye, Mouth, Accessory} from './AssetMap';
import {Preview, Settings} from './features';

export default function App() {
  const classes = useStyles();
  const {pageEvent} = useAnalyticsTracking();
  const [bodyLeft, setBodyLeft] = useState('#844093');
  const [bodyRight, setBodyRight] = useState('#603a8e');
  const [eyeLeft, setEyeLeft] = useState('default');
  const [eyeRight, setEyeRight] = useState('default');
  const [mouth, setMouth] = useState('default');
  const [accessory1, setAccessory1] = useState('none');
  const [accessory2, setAccessory2] = useState('none');

  const shuffleAssets = useCallback(() => {
    setBodyLeft(randomHexColour());
    setBodyRight(randomHexColour());
    setEyeLeft(randomFromObject(LeftEye));
    setEyeRight(randomFromObject(RightEye));
    setMouth(randomFromObject(Mouth));
    setAccessory1(randomFromObject(Accessory));
    setAccessory2(randomFromObject(Accessory));

    pageEvent({
      category: 'Settings',
      action: 'Shuffled',
    });
  }, [pageEvent]);

  return (
    <Grid container component="main" className={classes.container}>
      <CssBaseline />

      <Typography component="h1" variant="srOnly">
        Uniting Gamers
      </Typography>

      <a
        href="https://gitlab.com/uniting-gamers/gilley-builder"
        target="_blank"
        rel="noopener noreferrer"
        className={classes.contribute}
      >
        <div className={classes.gitlab}>
          <GitLabIcon />
        </div>
        <Typography component="h2" variant="body1">
          Join the community and contribute!{' '}
        </Typography>
      </a>

      <Preview
        bodyLeft={bodyLeft}
        bodyRight={bodyRight}
        eyeLeft={eyeLeft}
        eyeRight={eyeRight}
        mouth={mouth}
        accessory1={accessory1}
        accessory2={accessory2}
        onShuffle={shuffleAssets}
      />

      <Settings
        bodyLeft={bodyLeft}
        setBodyLeft={setBodyLeft}
        bodyRight={bodyRight}
        setBodyRight={setBodyRight}
        eyeLeft={eyeLeft}
        setEyeLeft={setEyeLeft}
        eyeRight={eyeRight}
        setEyeRight={setEyeRight}
        mouth={mouth}
        setMouth={setMouth}
        accessory1={accessory1}
        setAccessory1={setAccessory1}
        accessory2={accessory2}
        setAccessory2={setAccessory2}
      />
    </Grid>
  );
}

const useStyles = makeStyles(theme => ({
  container: {
    backgroundColor: '#FFFFFF',
    backgroundImage: "url('background.png')",
    backgroundPosition: 'top left',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundAttachment: 'fixed',
  },
  gitlab: {
    width: 25,
    marginRight: theme.spacing(),
  },
  contribute: {
    boxShadow: theme.shadows[5],
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    padding: theme.spacing(2),
    backgroundColor: '#FFFFFF',
    borderBottomRightRadius: 4,
    textDecoration: 'none',
    zIndex: 1,
    color: theme.palette.primary.main,
    [theme.breakpoints.down('xs')]: {
      borderBottomRightRadius: 0,
    },
  },
}));

function randomFromObject(object: any) {
  const keys = Object.keys(object);
  return keys[Math.floor(Math.random() * keys.length)];
}

function randomHexColour() {
  return `#${Math.floor(Math.random() * 16777215).toString(16)}`;
}
