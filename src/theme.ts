import {createMuiTheme} from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#603a8e',
    },
    secondary: {
      main: '#fff69f',
    },
  },
});

export default theme;
