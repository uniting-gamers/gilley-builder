import './index.css';
import 'normalize.css';
import React from 'react';
import ReactDOM from 'react-dom';
import {ThemeProvider} from '@material-ui/core/styles';
import {AnalyticsTrackingProvider} from './hooks/AnalyticsTracking';
import App from './App';
import * as serviceWorker from './serviceWorker';
import theme from './theme';

ReactDOM.render(
  <React.StrictMode>
    <AnalyticsTrackingProvider id="UA-169234413-1">
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </AnalyticsTrackingProvider>
  </React.StrictMode>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
