import React, {createContext, useEffect, useContext} from 'react';
import ReactGA from 'react-ga';

interface PageEvent {
  category: string;
  action: string;
  label?: string;
  value?: number;
}

interface Context {
  pageEvent(event: PageEvent): void;
  pageView(path: string): void;
}

const initialContext = {
  pageEvent: () => {},
  pageView: () => {},
};

const AnalyticsTrackingContext = createContext<Context>(initialContext);

interface Props {
  children: React.ReactNode;
  id: string;
}

export function AnalyticsTrackingProvider(props: Props) {
  const {id, children} = props;

  function trackRouteChange() {
    ReactGA.pageview(window.location.pathname + window.location.search);
  }

  useEffect(() => {
    ReactGA.initialize(id);
  }, [id]);

  useEffect(trackRouteChange, []);

  return (
    <AnalyticsTrackingContext.Provider
      value={{pageEvent: ReactGA.event, pageView: ReactGA.pageview}}
    >
      {children}
    </AnalyticsTrackingContext.Provider>
  );
}

export function useAnalyticsTracking() {
  return useContext(AnalyticsTrackingContext);
}
