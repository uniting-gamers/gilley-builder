import React, {useCallback, useState} from 'react';
import {saveSvgAsPng} from 'save-svg-as-png';
import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import {makeStyles} from '@material-ui/core/styles';
import DownloadIcon from '@material-ui/icons/GetApp';
import ShuffleIcon from '@material-ui/icons/Autorenew';
import {LeftEye, RightEye, Mouth, Accessory} from '../../AssetMap';
import {useAnalyticsTracking} from '../../hooks/AnalyticsTracking';

interface Props {
  bodyLeft: string;
  bodyRight: string;
  eyeLeft: string;
  eyeRight: string;
  mouth: string;
  accessory1: string;
  accessory2: string;
  onShuffle: () => void;
}

const SVG_ID = 'Gilley';
const PNG_FILE_NAME = 'my-gilley.png';

export default function Preview({
  bodyLeft,
  bodyRight,
  eyeLeft,
  eyeRight,
  mouth,
  accessory1,
  accessory2,
  onShuffle,
}: Props) {
  const classes = useStyles();
  const [downloading, setDownloading] = useState(false);
  const {pageEvent} = useAnalyticsTracking();

  const downloadAsPng = useCallback(async () => {
    setDownloading(true);

    const svgNode = document.getElementById(SVG_ID);
    await saveSvgAsPng(svgNode, PNG_FILE_NAME);

    pageEvent({
      category: 'Preview',
      action: 'Downloaded',
      label: formatSettings({
        bodyLeft,
        bodyRight,
        eyeLeft,
        eyeRight,
        mouth,
        accessory1,
        accessory2,
      }),
    });

    setDownloading(false);
  }, [
    pageEvent,
    bodyLeft,
    bodyRight,
    eyeLeft,
    eyeRight,
    mouth,
    accessory1,
    accessory2,
  ]);

  return (
    <Grid item xs={12} sm={4} md={7} className={classes.root}>
      <div className={classes.svg}>
        <svg id={SVG_ID} viewBox="0 0 500 500" width="500" height="500">
          <g id="Body">
            <polygon
              id="BodyLeft"
              points="321.12 174.64 321.12 156.27 302.75 156.27 302.75 142.78 250.29 142.78 197.83 142.78 197.83 156.27 179.45 156.27 179.45 174.64 161.08 174.64 161.08 357.95 208.76 357.95 208.76 341.67 225.75 341.67 225.75 357.95 250.29 357.95 275.06 357.95 275.06 341.67 292.05 341.67 292.05 357.95 339.5 357.95 339.5 174.64 321.12 174.64"
              fill={bodyLeft}
            />

            <polyline
              id="BodyRight"
              points="250.29 357.95 275.06 357.98 275.06 341.69 292.05 341.69 292.05 357.98 339.5 357.98 339.5 174.67 321.14 174.67 321.14 156.31 302.78 156.31 302.78 142.78 250.29 142.78"
              fill={bodyRight}
            />
          </g>

          <g id="Eyes">
            {LeftEye[eyeLeft].main}
            {RightEye[eyeRight].main}
          </g>

          <g id="Mouth">{Mouth[mouth].main}</g>

          <g id="Accessory1">{Accessory[accessory1].main}</g>

          <g id="Accessory2">{Accessory[accessory2].main}</g>
        </svg>
      </div>

      <div className={classes.actionlist}>
        <Fab
          color="primary"
          variant="extended"
          onClick={downloadAsPng}
          disabled={downloading}
          className={classes.action}
        >
          <DownloadIcon className={classes.extendedIcon} />
          Download PNG
        </Fab>

        <Fab
          color="secondary"
          variant="extended"
          onClick={onShuffle}
          disabled={downloading}
          className={classes.action}
        >
          <ShuffleIcon className={classes.extendedIcon} />
          Roll the dice
        </Fab>
      </div>
    </Grid>
  );
}

const useStyles = makeStyles(theme => ({
  root: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: theme.spacing(2),
    zIndex: 0,
    overflow: 'hidden',
    height: '100vh',
    [theme.breakpoints.down('xs')]: {
      height: 'auto',
    },
  },
  svg: {
    height: 500,
    top: '50%',
    marginTop: -300,
    position: 'fixed',
    [theme.breakpoints.down('xs')]: {
      top: 0,
      marginTop: 0,
      position: 'relative',
    },
  },
  actionlist: {
    bottom: theme.spacing(6),
    position: 'fixed',
    display: 'flex',
    [theme.breakpoints.down('sm')]: {
      bottom: theme.spacing(),
      flexDirection: 'column',
    },
    [theme.breakpoints.down('xs')]: {
      position: 'absolute',
    },
  },
  action: {
    marginRight: theme.spacing(2),
    ':last-child': {
      marginRight: 0,
    },

    [theme.breakpoints.down('sm')]: {
      marginRight: 0,
      marginBottom: theme.spacing(2),
      ':last-child': {
        marginBottom: 0,
      },
    },
  },
  extendedIcon: {
    marginRight: theme.spacing(),
  },
}));

interface SettingValue {
  [key: string]: string;
}

function formatSettings(settingsValues: SettingValue) {
  const keys = Object.keys(settingsValues);
  const keyValue = keys.map(key => `${key}:${settingsValues[key]}`);
  return keyValue.join('|');
}
