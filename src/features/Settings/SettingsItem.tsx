import React, {useCallback} from 'react';
import classNames from 'classnames';
import {makeStyles} from '@material-ui/core/styles';

interface Props {
  children: React.ReactNode;
  selected?: boolean;
  onClick: (value: string) => void;
  value: string;
}

export default function SettingsItem({
  children,
  selected,
  onClick,
  value,
}: Props) {
  const classes = useStyles();
  const className = classNames(selected && classes.selected, classes.button);

  const handleClick = useCallback(() => {
    onClick(value);
  }, [onClick, value]);

  return (
    <div className={classes.root}>
      <button type="button" onClick={handleClick} className={className}>
        <div className={classes.preview}>{children}</div>
      </button>
    </div>
  );
}

const useStyles = makeStyles(theme => ({
  selected: {
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.common.white,
  },
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    ...theme.typography.body1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 80,
    height: 80,
    border: 'none',
    borderRadius: '50%',
    padding: 0,
    margin: 0,
    [theme.breakpoints.down('md')]: {
      width: 60,
      height: 60,
    },
    [theme.breakpoints.down('sm')]: {
      width: 50,
      height: 50,
    },
    ':after': {
      content: '',
      display: 'block',
      paddingBottom: '100%',
    },
  },
  preview: {
    width: '50%',
  },
}));
