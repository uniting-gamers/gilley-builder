import React from 'react';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import {ColorPicker} from 'material-ui-color';
import {useAnalyticsTracking} from '../../hooks/AnalyticsTracking';
import {LeftEye, RightEye, Mouth, Accessory} from '../../AssetMap';

import SettingsRow from './SettingsRow';

type SetState = React.Dispatch<React.SetStateAction<string>>;

interface Props {
  bodyLeft: string;
  setBodyLeft: SetState;
  bodyRight: string;
  setBodyRight: SetState;
  eyeLeft: string;
  setEyeLeft: SetState;
  eyeRight: string;
  setEyeRight: SetState;
  mouth: string;
  setMouth: SetState;
  accessory1: string;
  setAccessory1: SetState;
  accessory2: string;
  setAccessory2: SetState;
}

export default function Settings({
  bodyLeft,
  setBodyLeft,
  bodyRight,
  setBodyRight,
  eyeLeft,
  setEyeLeft,
  eyeRight,
  setEyeRight,
  mouth,
  setMouth,
  accessory1,
  setAccessory1,
  accessory2,
  setAccessory2,
}: Props) {
  const classes = useStyles();
  const {pageEvent} = useAnalyticsTracking();

  return (
    <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
      <div className={classes.paper}>
        <form className={classes.form} noValidate>
          <Grid container>
            <Grid item xs={12} sm={6} md={6}>
              <FormControl>
                <FormLabel>Body left-side</FormLabel>
                <ColorPicker
                  onChange={color => {
                    const value = `#${color.hex}`;

                    setBodyLeft(value);

                    pageEvent({
                      category: 'Settings',
                      action: 'Changed BodyLeft',
                      label: value,
                    });
                  }}
                  value={bodyLeft}
                />
              </FormControl>
            </Grid>

            <Grid item xs={12} sm={6} md={6} className={classes.row}>
              <FormControl>
                <FormLabel>Body right-side</FormLabel>
                <ColorPicker
                  onChange={color => {
                    const value = `#${color.hex}`;

                    setBodyRight(value);

                    pageEvent({
                      category: 'Settings',
                      action: 'Changed BodyRight',
                      label: value,
                    });
                  }}
                  value={bodyRight}
                />
              </FormControl>
            </Grid>

            <SettingsRow
              label="Left eye"
              name="LeftEye"
              items={LeftEye}
              onChange={(value: string) => setEyeLeft(value)}
              value={eyeLeft}
            />

            <SettingsRow
              label="Right eye"
              name="RightEye"
              items={RightEye}
              onChange={(value: string) => setEyeRight(value)}
              value={eyeRight}
            />

            <SettingsRow
              label="Mouth"
              name="Mouth"
              items={Mouth}
              onChange={(value: string) => setMouth(value)}
              value={mouth}
            />

            <SettingsRow
              label="Accessory 1"
              name="Accessory1"
              items={Accessory}
              onChange={(value: string) => setAccessory1(value)}
              value={accessory1}
            />

            <SettingsRow
              label="Accessory 2"
              name="Accessory2"
              items={Accessory}
              onChange={(value: string) => setAccessory2(value)}
              value={accessory2}
            />
          </Grid>
        </form>
      </div>

      <a
        href="https://unitinggamers.com"
        target="_blank"
        rel="noopener noreferrer"
        className={classes.unitinggamers}
      >
        <Typography>by Uniting Gamers</Typography>
      </a>
    </Grid>
  );
}

const useStyles = makeStyles(theme => ({
  row: {
    [theme.breakpoints.down('xs')]: {
      marginTop: theme.spacing(8),
    },
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  title: {
    marginBottom: theme.spacing(6),
  },
  svg: {
    width: '100%',
  },
  unitinggamers: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: theme.spacing(6),
    textDecoration: 'none',
    color: theme.palette.primary.main,
  },
}));
