import React, {useCallback} from 'react';
import Carousel from 'react-elastic-carousel';
import {makeStyles} from '@material-ui/core/styles';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import {useAnalyticsTracking} from '../../hooks/AnalyticsTracking';
import SettingsItem from './SettingsItem';

interface Item {
  preview: React.ReactNode;
}

interface Props {
  label: string;
  name: string;
  items: {
    [key: string]: Item;
  };
  onChange: (value: string) => void;
  value: string;
}

export default function SettingsRow({
  label,
  name,
  items,
  onChange,
  value,
}: Props) {
  const classes = useStyles();
  const {pageEvent} = useAnalyticsTracking();

  const handleArrow = useCallback(
    ({type, onClick}) => {
      return (
        <div className={classes.arrow} onClick={onClick}>
          {type === 'PREV' ? <ArrowBackIosIcon /> : <ArrowForwardIosIcon />}
        </div>
      );
    },
    [classes.arrow],
  );

  const handleChange = useCallback(
    (value: string) => {
      onChange(value);
      pageEvent({
        category: 'Settings',
        action: `Changed ${name}`,
        label: value,
      });
    },
    [pageEvent, onChange, name],
  );

  const itemsMarkup = Object.keys(items).map(key => (
    <SettingsItem
      onClick={handleChange}
      value={key}
      selected={key === value}
      key={key}
    >
      {items[key].preview}
    </SettingsItem>
  ));

  return (
    <FormControl className={classes.root}>
      <FormLabel className={classes.label}>{label}</FormLabel>
      <Carousel pagination={false} itemsToShow={5} renderArrow={handleArrow}>
        {itemsMarkup}
      </Carousel>
    </FormControl>
  );
}

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(8),
    width: '100%',
  },
  label: {
    marginBottom: theme.spacing(),
  },
  arrow: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));
