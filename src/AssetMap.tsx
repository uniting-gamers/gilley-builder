import React from 'react';
import {ReactComponent as Bow} from './assets/accessories/bow.svg';
import {ReactComponent as BowPreview} from './assets/accessories/bow-preview.svg';
import {ReactComponent as Coffee} from './assets/accessories/coffee.svg';
import {ReactComponent as CoffeePreview} from './assets/accessories/coffee-preview.svg';
import {ReactComponent as Dragon} from './assets/accessories/dragon.svg';
import {ReactComponent as DragonPreview} from './assets/accessories/dragon-preview.svg';
import {ReactComponent as Glasses} from './assets/accessories/glasses.svg';
import {ReactComponent as GlassesPreview} from './assets/accessories/glasses-preview.svg';
import {ReactComponent as Hat} from './assets/accessories/hat.svg';
import {ReactComponent as HatPreview} from './assets/accessories/hat-preview.svg';
import {ReactComponent as Hp} from './assets/accessories/hp.svg';
import {ReactComponent as HpPreview} from './assets/accessories/hp-preview.svg';
import {ReactComponent as Love1} from './assets/accessories/love1.svg';
import {ReactComponent as Love1Preview} from './assets/accessories/love1-preview.svg';
import {ReactComponent as Love2} from './assets/accessories/love2.svg';
import {ReactComponent as Love2Preview} from './assets/accessories/love2-preview.svg';
import {ReactComponent as Mustache} from './assets/accessories/mustache.svg';
import {ReactComponent as MustachePreview} from './assets/accessories/mustache-preview.svg';
import {ReactComponent as Trees1} from './assets/accessories/trees1.svg';
import {ReactComponent as Trees1Preview} from './assets/accessories/trees1-preview.svg';
import {ReactComponent as Trees2} from './assets/accessories/trees2.svg';
import {ReactComponent as Trees2Preview} from './assets/accessories/trees2-preview.svg';
import {ReactComponent as Wings1} from './assets/accessories/wings1.svg';
import {ReactComponent as Wings1Preview} from './assets/accessories/wings1-preview.svg';

interface Asset {
  preview: React.ReactNode | null;
  main: React.ReactNode | null;
}

interface AssetList {
  [name: string]: Asset;
}

export const LeftEye: AssetList = {
  default: {
    preview: (
      <svg style={{width: '50%'}} viewBox="0 0 39.32 29.22">
        <polygon
          fill="#212121"
          points="39.32 19.66 29.77 29.22 19.66 19.11 9.55 29.22 0 19.67 19.66 0 39.32 19.66"
        />
      </svg>
    ),
    main: (
      <polygon
        fill="#212121"
        points="309 229.89 299.44 239.45 289.33 229.34 279.23 239.45 269.67 229.9 289.33 210.23 309 229.89"
      />
    ),
  },
  blink: {
    preview: (
      <svg style={{width: '50%'}} viewBox="0 0 39.32 9.55">
        <rect fill="#212121" width="39.32" height="9.55" />
      </svg>
    ),
    main: (
      <rect fill="#212121" x="269.67" y="229.9" width="39.32" height="9.55" />
    ),
  },
};

export const RightEye: AssetList = {
  default: {
    preview: (
      <svg style={{width: '50%'}} viewBox="0 0 39.32 29.22">
        <polygon
          fill="#212121"
          points="39.32 19.66 29.77 29.22 19.66 19.11 9.55 29.22 0 19.67 19.66 0 39.32 19.66"
        />
      </svg>
    ),
    main: (
      <polygon
        fill="#212121"
        points="231.14 229.89 221.58 239.45 211.48 229.34 201.37 239.45 191.81 229.9 211.47 210.23 231.14 229.89"
      />
    ),
  },
  blink: {
    preview: (
      <svg style={{width: '50%'}} viewBox="0 0 39.32 9.55">
        <rect fill="#212121" width="39.32" height="9.55" />
      </svg>
    ),
    main: (
      <rect fill="#212121" x="191.81" y="229.9" width="39.32" height="9.55" />
    ),
  },
};

export const Mouth: AssetList = {
  default: {
    preview: (
      <svg style={{width: '50%'}} viewBox="0 0 33.64 13.51">
        <rect fill="#212121" width="33.64" height="13.51" />
      </svg>
    ),
    main: (
      <rect fill="#212121" x="233.59" y="249.2" width="33.64" height="13.51" />
    ),
  },
  mouth2: {
    preview: (
      <svg style={{width: '50%'}} viewBox="0 0 33.2 3.06">
        <rect fill="#212121" width="33.2" height="3.06" />
      </svg>
    ),
    main: (
      <rect fill="#212121" x="233.81" y="254.42" width="33.2" height="3.06" />
    ),
  },
  mouth3: {
    preview: (
      <svg style={{width: '50%'}} viewBox="0 0 24.86 4.25">
        <rect fill="#212121" width="24.86" height="4.25" rx="2.12" />
      </svg>
    ),
    main: (
      <rect
        fill="#212121"
        x="237.98"
        y="253.83"
        width="24.86"
        height="4.25"
        rx="2.12"
      />
    ),
  },
  mouth4: {
    preview: (
      <svg style={{width: '50%'}} viewBox="0 0 29.58 7.06">
        <path
          fill="#212121"
          d="M1.22,0A1.22,1.22,0,0,0,0,1.22V3.06C0,4.35,2.08,5.52,5.62,6.25a46,46,0,0,0,9,.81c8.28,0,15-1.8,15-4V1.22A1.22,1.22,0,0,0,28.36,0Z"
        />
      </svg>
    ),
    main: (
      <path
        fill="#212121"
        d="M236.84,254.42a1.22,1.22,0,0,0-1.22,1.22v1.84c0,1.29,2.07,2.47,5.62,3.19a45.89,45.89,0,0,0,9,.82c8.28,0,15-1.81,15-4v-1.84a1.22,1.22,0,0,0-1.22-1.22Z"
      />
    ),
  },
  mouth5: {
    preview: (
      <svg style={{width: '50%'}} viewBox="0 0 25.55 8.23">
        <path
          fill="#212121"
          d="M3.06,0A3.06,3.06,0,0,0,0,3.06H0C0,4.35,1.8,6.69,4.86,7.41a34.37,34.37,0,0,0,7.75.82c7.15,0,12.94-3,12.94-5.17h0A3.05,3.05,0,0,0,22.5,0Z"
        />
      </svg>
    ),
    main: (
      <path
        fill="#212121"
        d="M240.7,254.42a3.06,3.06,0,0,0-3,3.06h0c0,1.29,1.79,3.63,4.86,4.36a34.35,34.35,0,0,0,7.75.81c7.15,0,12.94-3,12.94-5.17h0a3.06,3.06,0,0,0-3.06-3.06Z"
      />
    ),
  },
  mouth6: {
    preview: (
      <svg style={{width: '50%'}} viewBox="0 0 35.49 10.55">
        <path
          fill="#212121"
          d="M17.51,10.55c9.94,0,18-3.48,18-6.83V2.08A2,2,0,0,0,33.57,0H1.92A2,2,0,0,0,0,2.08H0V3.72C0,6.66,9.41,10.55,17.51,10.55Z"
        />
      </svg>
    ),
    main: (
      <path
        fill="#212121"
        d="M250.19,265c9.94,0,18-3.48,18-6.84v-1.63a2,2,0,0,0-1.92-2.09H234.6a2,2,0,0,0-1.92,2.09h0v1.63C232.68,261.08,242.08,265,250.19,265Z"
      />
    ),
  },
  mouth7: {
    preview: (
      <svg style={{width: '50%'}} viewBox="0 0 32.58 14.62">
        <path
          fill="#212121"
          d="M32.26,4.07A2.79,2.79,0,0,0,29.78,0h-27A2.79,2.79,0,0,0,.29,4c1.91,3.92,6.32,10.6,15.92,10.6S30.23,8,32.26,4.07Z"
        />
      </svg>
    ),
    main: (
      <path
        fill="#212121"
        d="M266.34,258.49a2.79,2.79,0,0,0-2.48-4.07h-27a2.79,2.79,0,0,0-2.5,4c1.91,3.92,6.33,10.6,15.92,10.6S264.31,262.41,266.34,258.49Z"
      />
    ),
  },
  mouth8: {
    preview: (
      <svg style={{width: '50%'}} viewBox="0 0 36.32 11.78">
        <path
          fill="#212121"
          d="M36,3.25A2.14,2.14,0,0,0,34.18,0h-32A2.14,2.14,0,0,0,.29,3.21c2,3.49,6.78,8.57,17.73,8.57S33.88,6.74,36,3.25Z"
        />
      </svg>
    ),
    main: (
      <path
        fill="#212121"
        d="M268.22,257.68a2.15,2.15,0,0,0-1.83-3.26h-32a2.14,2.14,0,0,0-1.85,3.21c2,3.49,6.78,8.58,17.73,8.58S266.09,261.16,268.22,257.68Z"
      />
    ),
  },
  mouth9: {
    preview: (
      <svg style={{width: '50%'}} viewBox="0 0 26.93 13.16">
        <path
          fill="#212121"
          d="M26.93,4c0,3.64-6,9.17-13.47,9.17S0,7.63,0,4,6,0,13.46,0,26.93.36,26.93,4Z"
        />
      </svg>
    ),
    main: (
      <path
        fill="#212121"
        d="M263.75,256.84c0,3.63-6,9.17-13.46,9.17s-13.47-5.54-13.47-9.17,6-4,13.47-4S263.75,253.21,263.75,256.84Z"
      />
    ),
  },
  mouth10: {
    preview: (
      <svg style={{width: '50%'}} viewBox="0 0 17.53 15.63">
        <path
          fill="#212121"
          d="M17.53,4.22c0,4.32-3.85,11.41-8.69,11.41S0,8.54,0,4.22,4,0,8.84,0,17.53-.09,17.53,4.22Z"
        />
      </svg>
    ),
    main: (
      <path
        fill="#212121"
        d="M259.05,256.7c0,4.32-3.85,11.41-8.69,11.41s-8.83-7.09-8.83-11.41,4-4.22,8.83-4.22S259.05,252.38,259.05,256.7Z"
      />
    ),
  },
  mouth11: {
    preview: (
      <svg style={{width: '50%'}} viewBox="0 0 12.6 16.94">
        <ellipse fill="#212121" cx="6.3" cy="8.47" rx="6.3" ry="8.47" />
      </svg>
    ),
    main: <ellipse fill="#212121" cx="250.29" cy="256.66" rx="6.3" ry="8.47" />,
  },
  mouth12: {
    preview: (
      <svg style={{width: '50%'}} viewBox="0 0 33.2 6.14">
        <polygon
          fill="#212121"
          points="33.2 0 33.2 0 0 0 0 3.06 5.92 3.06 5.77 6.14 8.82 3.06 10.42 3.06 10.42 6.14 13.48 3.06 15.07 3.06 15.07 6.14 18.13 3.06 19.72 3.06 19.72 6.14 22.78 3.06 24.38 3.06 24.38 6.14 27.43 3.06 33.2 3.06 33.2 0 33.2 0"
        />
      </svg>
    ),
    main: (
      <polygon
        fill="#212121"
        points="266.89 254.44 266.89 254.44 233.69 254.44 233.69 257.5 239.61 257.5 239.46 260.58 242.51 257.5 244.11 257.5 244.11 260.58 247.16 257.5 248.76 257.5 248.76 260.58 251.82 257.5 253.41 257.5 253.41 260.58 256.47 257.5 258.06 257.5 258.06 260.58 261.12 257.5 266.89 257.5 266.89 254.44 266.89 254.44"
      />
    ),
  },
  mouth13: {
    preview: (
      <svg style={{width: '50%'}} viewBox="0 0 17.53 15.63">
        <path
          fill="#212121"
          d="M0,9.08C0,4.76,3.85,0,8.69,0s8.84,4.76,8.84,9.08-4,6.55-8.84,6.55S0,13.4,0,9.08Z"
        />
      </svg>
    ),
    main: (
      <path
        fill="#212121"
        d="M241.53,261.56c0-4.32,3.84-9.08,8.68-9.08s8.84,4.76,8.84,9.08-4,6.55-8.84,6.55S241.53,265.87,241.53,261.56Z"
      />
    ),
  },
  mouth14: {
    preview: (
      <svg style={{width: '50%'}} viewBox="0 0 38.54 18.22">
        <path
          fill="#212121"
          d="M19.15,18.22c10.72,0,19.39-8.15,19.39-18.22H0C0,10.07,8.57,18.22,19.15,18.22"
        />
      </svg>
    ),
    main: (
      <path
        fill="#212121"
        d="M250.29,271.71c10.71,0,19.38-8.15,19.38-18.22H231.14c0,10.07,8.56,18.22,19.15,18.22"
      />
    ),
  },
  mouth15: {
    preview: (
      <svg style={{width: '50%'}} viewBox="0 0 37.05 33.04">
        <ellipse fill="#212121" cx="18.52" cy="16.52" rx="18.52" ry="16.52" />
      </svg>
    ),
    main: <ellipse fill="#212121" cx="250.29" cy="256.66" rx="6.3" ry="8.47" />,
  },
  mouth16: {
    preview: (
      <svg style={{width: '50%'}} viewBox="0 0 53.41 12.58">
        <polygon
          fill="#212121"
          points="0 4.11 12.97 0 26.7 4.35 40.43 0 53.41 4.11 26.71 12.58 0 4.11"
        />
      </svg>
    ),
    main: (
      <polygon
        fill="#212121"
        points="223.7 253.78 236.68 249.66 250.4 254.01 264.13 249.66 277.11 253.78 250.41 262.24 223.7 253.78"
      />
    ),
  },
};

export const Accessory: AssetList = {
  none: {
    preview: null,
    main: null,
  },
  glasses: {
    preview: <GlassesPreview />,
    main: <Glasses />,
  },
  bow: {
    preview: <BowPreview />,
    main: <Bow />,
  },
  coffee: {
    preview: <CoffeePreview />,
    main: <Coffee />,
  },
  dragon: {
    preview: <DragonPreview />,
    main: <Dragon />,
  },
  hat: {
    preview: <HatPreview />,
    main: <Hat />,
  },
  hp: {
    preview: <HpPreview />,
    main: <Hp />,
  },
  love1: {
    preview: <Love1Preview />,
    main: <Love1 />,
  },
  love2: {
    preview: <Love2Preview />,
    main: <Love2 />,
  },
  mustache: {
    preview: <MustachePreview />,
    main: <Mustache />,
  },
  trees1: {
    preview: <Trees1Preview />,
    main: <Trees1 />,
  },
  trees2: {
    preview: <Trees2Preview />,
    main: <Trees2 />,
  },
  wings1: {
    preview: <Wings1Preview />,
    main: <Wings1 />,
  },
};
